$(call inherit-product, vendor/google/pixelparts/touch/device.mk)

# Camera
PRODUCT_BROKEN_VERIFY_USES_LIBRARIES := true
PRODUCT_PACKAGES += \
    GoogleCamera

# Dolby
BOARD_VENDOR_SEPOLICY_DIRS += \
    vendor/google/pixelparts/sepolicy/dolby

# Parts
PRODUCT_PACKAGES += \
    GoogleParts \
    PixelFrameworksOverlay \
    TurboAdapter

ifeq ($(PRODUCT_SUPPORTS_CLEAR_CALLING), true)
PRODUCT_PACKAGES += \
    ClearCallingSettingsOverlay
endif
